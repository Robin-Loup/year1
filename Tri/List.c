#include "List.h"

void swap_integer (int *a , int *b){
    int tmp ;

    tmp = *a;
    *a = *b;
    *b = tmp;
}

void swap_mem(void* z1, void* z2, size_t size) {
    void *tmp = malloc(size);

    if (tmp) {
        memcpy(tmp, z1, size);
        memcpy(z1, z2, size);
        memcpy(z2, tmp, size);
        free(tmp);
    }
}

Cell* allocate_cell(char *first, char *last, int age){
    Cell *new_cell = (Cell *)malloc(sizeof(Cell));

    if(!new_cell){
        exit(EXIT_FAILURE);
    }

    new_cell->first_name = (char*)malloc(sizeof(char));
    new_cell->last_name = (char*)malloc(sizeof(char));

    strcpy(new_cell->first_name, first);
    strcpy(new_cell->last_name, last);
    new_cell->age = age;
    new_cell->next = NULL;

    return new_cell;

}

int age_order(Cell* p1, Cell* p2){
    return p1->age < p2->age;
}

int name_order(Cell* p1, Cell* p2){
    int i;
    /*First_name*/
    for (i = 0; p1->first_name[i] != '\0'; i++){
        if (p2->first_name[i] == '\0' || p1->first_name[i] > p2->first_name[i]){
            return 0;
        }
        if (p1->first_name[i] < p2->first_name[i]){
            return 1;
        }
    }
    for (i = 0; p1->last_name[i] != '\0'; i++){
        if (p2->last_name[i] == '\0' || p1->last_name[i] > p2->last_name[i]){
            return 0;
        }
        if (p1->last_name[i] < p2->last_name[i]){
            return 1;
        }
    }
    return 0;
}

void ordered_insertion(List* l, Cell* new, int order_func(Cell*, Cell*)){
    if (*l == NULL){
        *l = new;
    }
    else if(order_func(*l, new) == 0){
        new->next = *l;
        *l = new;
    }
    else{
        if ((*l)->next == NULL){
            (*l)->next = new;
        }
        else {
            ordered_insertion(&((*l)->next), new, order_func);
        }
    }
}

void print_list(List l){
    if(l != NULL){
        List tmp = l;
        printf("%s %s %d\n", tmp->first_name, tmp->last_name, tmp->age);
        tmp = tmp->next;
        print_list(tmp);
    }
}

void free_list(List l){
    if(NULL != l){
        List tmp = l;
        free(tmp);
        free_list(l->next);
    }
}
