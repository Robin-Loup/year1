#include "List.h"

int main(int argc, char* argv[]) {

    char* first;
    char* last;
    int age;
    Cell* tmp;
    FILE *fichier;
    List l = NULL;

    if(argc < 2)
        exit(1);

    if (!(fichier = fopen("liste_nom.txt", "r"))) {
        printf("Impossible d'ouvrir le fichier\n");
        exit(1);
    }

    first = (char*)malloc(sizeof(char));
    last = (char*)malloc(sizeof(char));

    while(fscanf(fichier,"%s %s %d",first, last, &age) != EOF){
        tmp = allocate_cell(first, last, age);
        ordered_insertion(&l, tmp, name_order);
    }

    print_list(l);
    free_list(l);
    free(first);
    free(last);
    fclose(fichier);

    return 0;
}