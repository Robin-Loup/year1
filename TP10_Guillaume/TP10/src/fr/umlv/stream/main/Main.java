package fr.umlv.stream.main;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main {

	public static void main(String[] args) {
		var list = List.of("hello", "world", "hello", "lambda");
	     System.out.println(count2(list, "hello"));  // 2
	     
	     var list2 = List.of("hello", "world", "hello", "lambda");
	     System.out.println(upperCase(list2));  // [HELLO, WORLD, HELLO, LAMBDA]
	}
	
	private static long count (List<String> list, String string) {
		Stream<String> st = list.stream();
		Stream<String> st2 = st.filter(S -> S.equals(string));
		return st2.count();
	}
	
	private static long count2 (List<String> list, String string) {
		int count = 0;
		for (Object object : list) {
			if(object.equals(string))
				count++;
		}
		return count;
	}
	
	private static List<String> upperCase (List<String> list){
		List<String> list2 = new ArrayList<String>();
		for (String string : list) {
			list2.add(string.toUpperCase());
		}
		return list2;
	}

	
	private static List<String> upperCase2 (List<String> list){
		return list.stream().map(s -> s.toUpperCase()).collect(Collectors.toList());
	}
	
	private static List<String> upperCase3 (List<String> list){
		return list.stream().map(String::toUpperCase).collect(Collectors.toList());
	}
	
	private static List<String> upperCase4 (List<String> list){
		return list.stream().map(s -> s.toUpperCase()).collect(Collectors.toList());
	}
}
